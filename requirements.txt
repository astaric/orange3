numpy>=1.6.2
scipy>=0.11.0
-e hg+https://bitbucket.org/janezd/bottlechest#egg=bottleneck-dev
-e git+https://github.com/scikit-learn/scikit-learn@a88ee541f27330a0877cae2cac9a5b0986865035#egg=scikit-learn-dev
nose==1.2.1
mock>=1.0.1
